import numpy as np
import gdal
import osr
from urlparse import urlparse
from pyproj import Proj, transform
import affine

import os
import sys
sys.path.append(os.getcwd())
sys.path.append('/opt/OTB/lib/python')
sys.path.append('/opt/OTB/lib/libfftw3.so.3')
os.environ['OTB_APPLICATION_PATH'] = '/opt/OTB/lib/otb/applications'
os.environ['LD_LIBRARY_PATH'] = '/opt/OTB/lib'
os.environ['ITK_AUTOLOAD_PATH'] = '/opt/OTB/lib/otb/applications'
import otbApplication


def retrieve_pixel_value_signature(geo_coord, data_source):
    
    """Return the signature"""
    
    epsg_code = osr.SpatialReference(wkt=data_source.GetProjection()).GetAttrValue('AUTHORITY',1)
    
    
    inProj = Proj(init='epsg:4326')
    outProj = Proj(init='epsg:{0}'.format(epsg_code))
    x_latlon, y_latlon = geo_coord[0], geo_coord[1]
    
    x, y = transform(inProj, outProj, x_latlon, y_latlon)
 
    forward_transform = affine.Affine.from_gdal(*data_source.GetGeoTransform())
    reverse_transform = ~forward_transform
    
    px, py = reverse_transform * (x, y)

    px, py = int(px + 0.5), int(py + 0.5)
    pixel_coord = px, py
    # xoff, yoff, xcount, ycount  
    
    signature = dict()
    
    signature['wkt'] = 'POINT({0} {1})'.format(x, y) 

    signature['pixel_coord_x'] = px
    signature['pixel_coord_y'] = py
    signature['epsg'] = 'EPSG:{0}'.format(epsg_code)
    
    for band in range(data_source.RasterCount):
        
        band += 1

        src_band = data_source.GetRasterBand(band)
    
        data_array = np.array(src_band.ReadAsArray(px, py, 1, 1))
        try:
            signature[src_band.GetDescription()] = data_array[0][0]
    
        except IndexError:
            
            continue
            
    return signature


def get_vsi_url(enclosure, user, api_key):
    
    
    parsed_url = urlparse(enclosure)

    url = '/vsicurl/%s://%s:%s@%s/api%s' % (list(parsed_url)[0],
                                            user, 
                                            api_key, 
                                            list(parsed_url)[1],
                                            list(parsed_url)[2])
    
    return url 


def analyse_row(row, geo_point, user, api_key):
    
    src_ds = gdal.Open(get_vsi_url(row['enclosure'], 
                                   user,
                                   api_key))
    

    signature = retrieve_pixel_value_signature([geo_point.x, geo_point.y], src_ds)
    
    signature['enclosure'] = row['enclosure']
    signature['startdate'] = row['startdate']
    signature['cc'] = row['cc']
    
    return signature

def get_image_as_mem(enclosure, user, api_key, bbox, proj):
    
    
    parsed_url = urlparse(enclosure)

    url = '/vsicurl/%s://%s:%s@%s/api%s' % (list(parsed_url)[0],
                                            user, 
                                            api_key, 
                                            list(parsed_url)[1],
                                            list(parsed_url)[2])
    
    #[-projwin ulx uly lrx lry]
    ulx, uly, lrx, lry = bbox[0], bbox[3], bbox[2], bbox[1] 
    
    # load VSI URL in memory
    output = '/vsimem/subset.tif'
    
    ds = gdal.Open(url)
    ds = gdal.Translate(destName=output, 
                        srcDS=ds, 
                        projWin = [ulx, uly, lrx, lry], 
                        projWinSRS = proj,
                        outputType=gdal.GDT_Float32)
    ds = None
    
    # create a numpy array
    ds = gdal.Open(output)
    
    layers = []

    for i in range(1, ds.RasterCount+1):
        layers.append(ds.GetRasterBand(i).ReadAsArray())

    return np.dstack(layers)
    
    
def create_mem_composite(band_expression, data, hfact=2.0):
    
    BandMathX = otbApplication.Registry.CreateApplication("BandMathX")
    BandMathX.SetVectorImageFromNumpyArray('il', data)
    BandMathX.SetParameterString('exp', ';'.join(band_expression))
    BandMathX.Execute()
    
    Convert = otbApplication.Registry.CreateApplication("Convert")
    Convert.SetVectorImageFromNumpyArray('in', BandMathX.GetVectorImageAsNumpyArray('out'))
    Convert.SetParameterString("type","linear")
    Convert.SetParameterString("channels","rgb")
    Convert.Execute()
    
    ContrastEnhancement = otbApplication.Registry.CreateApplication("ContrastEnhancement")
    ContrastEnhancement.SetVectorImageFromNumpyArray("in", Convert.GetVectorImageAsNumpyArray('out'))
    ContrastEnhancement.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    ContrastEnhancement.SetParameterFloat("nodata", 0.0)
    ContrastEnhancement.SetParameterFloat("hfact", hfact)
    ContrastEnhancement.SetParameterInt("bins", 256)
    ContrastEnhancement.SetParameterInt("spatial.local.w", 100)
    ContrastEnhancement.SetParameterInt("spatial.local.h", 100)
    ContrastEnhancement.SetParameterString("mode","lum")
    ContrastEnhancement.Execute()
    
    return np.uint8(ContrastEnhancement.GetVectorImageAsNumpyArray('out'))