## ASTER analysis for Jones Lake

### Getting the experiment

This experiment is hosted in a software repository.

Use `git` to clone it:

```bash
cd /workspace
git clone https://gitlab.com/ellip/ifpen/notebooks/aster-analysis.git
cd aster-analysis
```

### Configuring the Python conda environment

The file `environment.yml` contains the Python conda environment for running the notebooks contained in this folder.

From the shell, run:

```bash
conda env create --file=environment.yml
```

Once the environment configuration is done, activate it:

```bash
conda activate aster-analisys
```

### Running the experiment

Open the `ASTER analysis for Jones Lake.ipynb` notebook and update the kernel to use `aster-analisys`

Run the experiment by executing each of the cells with `shift` + `Enter`.

When asked for the credentials, provide your Ellip username and associated Ellip API key.

### Improving the experiment in a development branch

This experiment is under version control and uses the git flow method (see [https://datasift.github.io/gitflow/IntroducingGitFlow.html])

If not done previously, clone the experiment repository:

```bash
git clone https://gitlab.com/ellip/ifpen/notebooks/aster-analysis.git
cd aster-analysis
```

Then, checkout the `develop` branch with:

```bash
git checkout develop
```

At this stage, update the experiment.

When done:

```bash
git add -A
git commit -m '<commit message>'
git pull
```

Finally, do a release with:

```bash
ciop-release
```





